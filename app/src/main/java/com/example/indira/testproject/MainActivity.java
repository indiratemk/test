package com.example.indira.testproject;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";


    private static final int COLUMN_WIDTH = 250;
    private int numColumns;

    private SearchView searchField;
    private RecyclerView recyclerView;
    private TextView showErrorMessageTextView;
    private MovieAdapter adapter;

    class MovieQueryTask extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            String response = null;
            try {
                response = NetworkUtils.getResponseFromUrl(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }


        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                Toast.makeText(MainActivity.this,
                        "Please connect to the Internet", Toast.LENGTH_SHORT).show();
                return;
            }
            List<Movie> movies = getMovies(response);
            numColumns = 1;
//            recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, numColumns));
//
//            recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    if (recyclerView.getWidth()/COLUMN_WIDTH != numColumns) {
//                        int newNumColumns = recyclerView.getWidth()/COLUMN_WIDTH;
//                        GridLayoutManager layoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
//                        layoutManager.setSpanCount(newNumColumns);
//                    }
//                }
//            });

            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(MainActivity.this);
            layoutManager.setFlexDirection(FlexDirection.ROW);
            layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
            recyclerView.setLayoutManager(layoutManager);

            Log.d(TAG, "onCreate: size: " + recyclerView.getWidth());

            recyclerView.setHasFixedSize(true);
            adapter = new MovieAdapter(MainActivity.this);
            adapter.setMovieList(movies);
            recyclerView.setAdapter(adapter);
        }


        public List<Movie> getMovies(String response) {
            List<Movie> moviesList = new ArrayList<>();
            try {
                JSONObject jsonResponse = new JSONObject(response);
                if (jsonResponse.getString("Response").equals("False")) {
                    showErrorMessageTextView.setText(jsonResponse.getString("Error"));
                    showErrorMessageTextView.setVisibility(View.VISIBLE);
                } else {
                    showErrorMessageTextView.setVisibility(View.GONE);
                    JSONArray moviesArray = jsonResponse.getJSONArray("Search");
                    String id;
                    String title;
                    String year;
                    String poster;
                    for (int i = 0; i < moviesArray.length(); i++) {
                        id = moviesArray.getJSONObject(i).getString("imdbID");
                        title = moviesArray.getJSONObject(i).getString("Title");
                        year = moviesArray.getJSONObject(i).getString("Year");
                        poster = moviesArray.getJSONObject(i).getString("Poster");
                        moviesList.add(new Movie(id, title, year, poster));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return moviesList;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate: " + R.id.tv_show_error_message);

        showErrorMessageTextView = findViewById(R.id.tv_show_error_message);
        Log.d(TAG, "onCreate: textview is " + showErrorMessageTextView);


        recyclerView = findViewById(R.id.rv_movies);
        Log.d(TAG, "onCreate: recycler view is" + recyclerView);

        searchField = findViewById(R.id.sv_movie_title);
        searchField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchField.setIconified(false);
            }
        });
        searchField.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                URL url = NetworkUtils.generateURL(s);
                MovieQueryTask movieQueryTask = new MovieQueryTask();
                movieQueryTask.execute(url);
                searchField.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }
}
