package com.example.indira.testproject;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> {
    private static final String TAG = "MovieAdapter";
    public static final String EXTRA_ID = "com.example.indira.movietest.EXTRA_ID";

    private List<Movie> movies;
    private Context context;

    public MovieAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_item, viewGroup, false);
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder movieHolder, int i) {
        Movie movie = movies.get(i);
        movieHolder.title.setText(movie.getTitle());
        movieHolder.year.setText(movie.getYear());
        Picasso.get().load(movie.getPoster()).into(movieHolder.image);

    }

    @Override
    public int getItemCount() {
        if (movies != null) {
            return movies.size();
        }
        return 0;
    }

    public void setMovieList(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }


    class MovieHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private TextView year;
        private ImageView image;
        private Button buttonShowInfo;


        public MovieHolder(@NonNull final View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_movie_title);
            year = itemView.findViewById(R.id.tv_movie_year);
            image = itemView.findViewById(R.id.iv_movie_poster);
            buttonShowInfo = itemView.findViewById(R.id.b_show_more);

            buttonShowInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, MovieInfoActivity.class);
                    intent.putExtra(EXTRA_ID, movies.get(getAdapterPosition()).getId());
                    context.startActivity(intent);
                }
            });

        }
    }
}
