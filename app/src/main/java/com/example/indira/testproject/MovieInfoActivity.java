package com.example.indira.testproject;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;


public class MovieInfoActivity extends AppCompatActivity {
    private ImageView posterImageView;
    private TextView titleTextView;
    private TextView productionTextView;
    private TextView actorsTextView;
    private TextView releasedTextView;
    private TextView directorTextView;
    private TextView countryTextView;
    private TextView durationTextView;
    private TextView ratedTextView;
    private TextView ratingsTextView;
    private TextView plotTextView;

    private GridLayout genresContainer;


    private Button closeButton;
    private RelativeLayout moreInfoRelativeLayout;


    class MovieMoreInfoAsyncTask extends AsyncTask<URL, Void, String> {

        @Override
        protected String doInBackground(URL... urls) {
            String response = null;
            try {
                response = NetworkUtils.getResponseFromUrl(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                return;
            }
            fillMovieInfoFields(response);
        }

        public void fillMovieInfoFields(String response) {
            try {
                JSONObject jsonResponse = new JSONObject(response);
                titleTextView.setText(jsonResponse.getString("Title"));
                productionTextView.setText(jsonResponse.getString("Production"));
                actorsTextView.setText(jsonResponse.getString("Actors"));
                releasedTextView.setText(jsonResponse.getString("Released"));
                directorTextView.setText(jsonResponse.getString("Director"));
                countryTextView.setText(jsonResponse.getString("Country"));
                durationTextView.setText(jsonResponse.getString("Runtime"));
                ratedTextView.setText(jsonResponse.getString("Rated"));

                String genres = jsonResponse.getString("Genre");
                for (String genre : genres.split(",")) {
                    TextView genreTextView = new TextView(MovieInfoActivity.this);
                    genreTextView.setText(genre);
                    genreTextView.setBackground(getResources()
                            .getDrawable(R.drawable.genres_background));
                    genreTextView.setTextColor(getResources().getColor(R.color.colorLogo));
                    genreTextView.setPadding(15, 7, 15, 7);
                    genresContainer.addView(genreTextView);
                }

                JSONArray ratingsArray = jsonResponse.getJSONArray("Ratings");
                for (int i = 0; i < ratingsArray.length(); i++) {
                    String source = ratingsArray.getJSONObject(i).getString("Source");
                    String value = ratingsArray.getJSONObject(i).getString("Value");
                    ratingsTextView.append(source + " " + value + "\n");
                }

                plotTextView.setText(jsonResponse.getString("Plot"));

                Picasso.get()
                        .load(jsonResponse.getString("Poster"))
//                        .resize(moreInfoRelativeLayout.getWidth(), 800)
//                        .centerInside()
//                        .transform(new RoundedTransformation(50, 50))
                        .resize(moreInfoRelativeLayout.getWidth(), 0)
                        .into(posterImageView);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_info);

        titleTextView = findViewById(R.id.tv_movie_title);
        productionTextView = findViewById(R.id.tv_movie_production);
        actorsTextView = findViewById(R.id.tv_movie_actors);
        releasedTextView = findViewById(R.id.tv_movie_released);
        directorTextView = findViewById(R.id.tv_movie_director);
        countryTextView = findViewById(R.id.tv_movie_country);
        durationTextView = findViewById(R.id.tv_movie_duration);
        ratedTextView = findViewById(R.id.tv_movie_rated);

        ratingsTextView = findViewById(R.id.tv_movie_ratings);
        plotTextView = findViewById(R.id.tv_movie_plot);

        genresContainer = findViewById(R.id.gl_movie_genres);

        posterImageView = findViewById(R.id.iv_movie_poster);

        closeButton = findViewById(R.id.b_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        moreInfoRelativeLayout = findViewById(R.id.rl_movie_more_info);


        Intent intent = getIntent();
        String id = intent.getStringExtra(MovieAdapter.EXTRA_ID);

        URL url = NetworkUtils.generateMoreInfoURL(id);

        new MovieMoreInfoAsyncTask().execute(url);
    }
}
